
# Credits

This is forked from https://github.com/Awesome-Technologies/synapse-admin?tab=readme-ov-file and just modified slightly for our needs

# Usage

to build the container:

```bash
docker buildx build --platform linux/amd64 -t synapse-admin --tag synapse-admin:x86 .
```

then tag it:

```bash
docker tag synapse-admin:latest codeberg.org/dex/synapse_admin:latest
```

then push it:

```bash
docker push codeberg.org/dex/synapse_admin:latest
```
